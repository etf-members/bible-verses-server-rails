# Base image:
FROM ruby:2.6.3

# Install dependencies
RUN apt-get update -qq && apt-get install -y build-essential nodejs cron libxslt-dev libxml2-dev
RUN apt-get install default-libmysqlclient-dev
RUN gem install mysql2 -v '0.5.3'

# Set an environment variable where the Rails app is installed to inside of Docker image:
ENV RAILS_ROOT /var/www/application
ENV RAILS_ENV development
ENV RAILS_LOG_TO_STDOUT true

RUN mkdir -p $RAILS_ROOT

# Set working directory, where the commands will be ran:
WORKDIR $RAILS_ROOT

COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock
RUN gem install bundler
RUN gem install rails -v 6.0.2
RUN bundle install
COPY . .
EXPOSE 3000

RUN mkdir -p $RAILS_ROOT/tmp/pids && mkdir -p $RAILS_ROOT/log && touch $RAILS_ROOT/log/crono.log

# The default command that gets ran will be to start the Puma server.
CMD rails db:migrate && bundle exec puma -C config/puma.rb