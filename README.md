

# Bible Verses API

### What is this repository for? ###

This is repository for my first freelance project on upwork. It contains code for the API of the project.

API is written in ***Ruby on Rails*** framework (framework for ***Ruby*** language). Covered with rspec specs, from where entire api documentation can be generated (see the example below).

  

### What is the idea behind Bible Verses? ###

As client asked, he wanted an application for the **KJV Bible (King James Version Bible)**. Wanted to have several features related to verses (*most important to be noted below*):

  

- **random verse from the bible**: spin the wheel to get the random verse from the Bible

- **random daily verse**: generated every day in the same time, with option to see previous days

- **daily verse subscription**: subscribe to get an email when daily verse is generated and receive that generated verse

- **reading plans**: ability to subscribe to a reading plan of the **Bible**, with an options to read **Bible** within **30** or **90** days, or **custom**. Selecting the days when we want to receive an email with Bible verses and complete percentage. When plan is created, emails will be received daily followed by plan and containing specific amount of verses, until entire **Bible** is **sent** (read)

  

### Components ###

  

* Models

* Views
	* using ***active serializer*** for responses, pure and clean
	#### bible_serializer.rb ####
			class BibleSerializer < ActiveModel::Serializer  
			  attribute :id  
			  attribute :index  
			  attribute :slug  
			  attribute :book_id  
			  attribute :chapter  
			  attribute :verse  
			  attribute :text  
			end
* Controllers

* Actions

	* making controller and model implementation pure clear, entire logic in action, example:
	#### bibles_controller.rb ####
	  	def search_by_book_or_verse
	    		@bibles = Bible.search_by_book_or_verse(params)

	    		render json: @bibles, status: :ok	
	  	end
	#### bible_model.rb ####
			def self.search_by_book_or_verse(params)  
			  Actions::Bibles::SearchByBookOrVerse.execute(params)  
			end
	#### actions/bibles/search_by_book_or_verse.rb ####
			module Actions
			  module Bibles

			    class SearchByBookOrVerse < ApplicationAction

			      def initialize(params)
			        super(params)

			        @search_param = params[:text]
			        @search_book = nil
			      end

			      def execute
			        Book.all.each do |book|
			          if @search_param.downcase.include? book.full_name.downcase
			            @search_book = book
			            break
			          end
			        end

			        if @search_book.nil?
			          []
			        elsif @search_param.downcase.split(@search_book.full_name.downcase).count > 1
			          chapter_verse = @search_param.downcase.split(@search_book.full_name.downcase).last.split(':')
			          chapter = chapter_verse.first unless chapter_verse.nil?
			          verse = chapter_verse.last unless chapter_verse.nil?

			          Rails.cache.fetch("bible_search_by_book_or_verses#{@search_param}", expires_in: 1.day) do
			            JSON.parse(Bible.where(book_id: @search_book.id, chapter: chapter, verse: verse).to_json, object_class: OpenStruct)
			          end
			        else
			          Rails.cache.fetch("bible_search_by_book_or_verses#{@search_param}", expires_in: 1.day) do
			            JSON.parse(Bible.where(book_id: @search_book.id).to_json, object_class: OpenStruct)
			          end
			        end
			      end

			    end
			  end
			end
### RSpec API Documentation ###

Auto-generated API Documentation from **rspec specs**.

***note**: open in swagger editor*

    {  
      "swagger": "2.0",  
      "info": {  
        "title": "OpenAPI Specification",  
      "description": "This is a sample server Petstore server.",  
      "termsOfService": "http://open-api.io/terms/",  
      "contact": {  
          "name": "API Support",  
      "url": "http://www.open-api.io/support",  
      "email": "support@open-api.io"  
      },  
      "license": {  
          "name": "Apache 2.0",  
      "url": "http://www.apache.org/licenses/LICENSE-2.0.html"  
      },  
      "version": "1.0.0"  
      },  
      "host": "localhost:3000",  
      "schemes": [  
        "http",  
      "https"  
      ],  
      "consumes": [  
        "application/json",  
      "application/xml"  
      ],  
      "produces": [  
        "application/json",  
      "application/xml"  
      ],  
      "paths": {  
        "/bibles/": {  
          "get": {  
            "tags": [  
              "Bibles"  
      ],  
      "summary": "",  
      "description": "",  
      "consumes": [  
              "application/json"  
      ],  
      "produces": [  
              "application/json"  
      ],  
      "parameters": [  
      
            ],  
      "responses": {  
              "200": {  
                "description": "returns list of bibles",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                  "Content-Type": {  
                    "description": "",  
      "type": "string",  
      "x-example-value": "application/json; charset=utf-8"  
      }  
                },  
      "examples": {  
                }  
              }  
            },  
      "deprecated": false,  
      "security": [  
      
            ]  
          }  
        },  
      "/bibles/{id}": {  
          "get": {  
            "tags": [  
              "Bibles"  
      ],  
      "summary": "",  
      "description": "",  
      "consumes": [  
              "application/json"  
      ],  
      "produces": [  
              "application/json"  
      ],  
      "parameters": [  
              {  
                "name": "id",  
      "in": "path",  
      "description": "",  
      "required": true,  
      "type": "integer"  
      }  
            ],  
      "responses": {  
              "200": {  
                "description": "returns bibles",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                  "Content-Type": {  
                    "description": "",  
      "type": "string",  
      "x-example-value": "application/json; charset=utf-8"  
      }  
                },  
      "examples": {  
                }  
              },  
      "404": {  
                "description": "returns 404 when bibles not found",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                  "Content-Type": {  
                    "description": "",  
      "type": "string",  
      "x-example-value": "application/json; charset=utf-8"  
      }  
                },  
      "examples": {  
                }  
              }  
            },  
      "deprecated": false,  
      "security": [  
      
            ]  
          }  
        },  
      "/books/genre": {  
          "get": {  
            "tags": [  
              "Books"  
      ],  
      "summary": "",  
      "description": "",  
      "consumes": [  
              "application/json"  
      ],  
      "produces": [  
              "application/json"  
      ],  
      "parameters": [  
      
            ],  
      "responses": {  
              "200": {  
                "description": "returns list of books",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                  "Content-Type": {  
                    "description": "",  
      "type": "string",  
      "x-example-value": "application/json; charset=utf-8"  
      }  
                },  
      "examples": {  
                }  
              }  
            },  
      "deprecated": false,  
      "security": [  
      
            ]  
          }  
        },  
      "/books/": {  
          "get": {  
            "tags": [  
              "Books"  
      ],  
      "summary": "",  
      "description": "",  
      "consumes": [  
              "application/json"  
      ],  
      "produces": [  
              "application/json"  
      ],  
      "parameters": [  
      
            ],  
      "responses": {  
              "200": {  
                "description": "returns list of books",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                  "Content-Type": {  
                    "description": "",  
      "type": "string",  
      "x-example-value": "application/json; charset=utf-8"  
      }  
                },  
      "examples": {  
                }  
              }  
            },  
      "deprecated": false,  
      "security": [  
      
            ]  
          }  
        },  
      "/books/new_testament": {  
          "get": {  
            "tags": [  
              "Books"  
      ],  
      "summary": "",  
      "description": "",  
      "consumes": [  
              "application/json"  
      ],  
      "produces": [  
              "application/json"  
      ],  
      "parameters": [  
      
            ],  
      "responses": {  
              "200": {  
                "description": "returns list of books",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                  "Content-Type": {  
                    "description": "",  
      "type": "string",  
      "x-example-value": "application/json; charset=utf-8"  
      }  
                },  
      "examples": {  
                }  
              }  
            },  
      "deprecated": false,  
      "security": [  
      
            ]  
          }  
        },  
      "/books/old_testament": {  
          "get": {  
            "tags": [  
              "Books"  
      ],  
      "summary": "",  
      "description": "",  
      "consumes": [  
              "application/json"  
      ],  
      "produces": [  
              "application/json"  
      ],  
      "parameters": [  
      
            ],  
      "responses": {  
              "200": {  
                "description": "returns list of books",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                  "Content-Type": {  
                    "description": "",  
      "type": "string",  
      "x-example-value": "application/json; charset=utf-8"  
      }  
                },  
      "examples": {  
                }  
              }  
            },  
      "deprecated": false,  
      "security": [  
      
            ]  
          }  
        },  
      "/books/{id}": {  
          "get": {  
            "tags": [  
              "Books"  
      ],  
      "summary": "",  
      "description": "",  
      "consumes": [  
              "application/json"  
      ],  
      "produces": [  
              "application/json"  
      ],  
      "parameters": [  
              {  
                "name": "id",  
      "in": "path",  
      "description": "",  
      "required": true,  
      "type": "integer"  
      }  
            ],  
      "responses": {  
              "200": {  
                "description": "returns book",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                  "Content-Type": {  
                    "description": "",  
      "type": "string",  
      "x-example-value": "application/json; charset=utf-8"  
      }  
                },  
      "examples": {  
                }  
              },  
      "404": {  
                "description": "returns 404 when book not found",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                  "Content-Type": {  
                    "description": "",  
      "type": "string",  
      "x-example-value": "application/json; charset=utf-8"  
      }  
                },  
      "examples": {  
                }  
              }  
            },  
      "deprecated": false,  
      "security": [  
      
            ]  
          }  
        },  
      "/genres/{id}": {  
          "get": {  
            "tags": [  
              "Genres"  
      ],  
      "summary": "",  
      "description": "",  
      "consumes": [  
              "application/json"  
      ],  
      "produces": [  
              "application/json"  
      ],  
      "parameters": [  
              {  
                "name": "id",  
      "in": "path",  
      "description": "",  
      "required": true,  
      "type": "integer"  
      }  
            ],  
      "responses": {  
              "200": {  
                "description": "returns genres",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                  "Content-Type": {  
                    "description": "",  
      "type": "string",  
      "x-example-value": "application/json; charset=utf-8"  
      }  
                },  
      "examples": {  
                }  
              },  
      "404": {  
                "description": "returns 404 when genres not found",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                  "Content-Type": {  
                    "description": "",  
      "type": "string",  
      "x-example-value": "application/json; charset=utf-8"  
      }  
                },  
      "examples": {  
                }  
              }  
            },  
      "deprecated": false,  
      "security": [  
      
            ]  
          }  
        },  
      "/popular_topics/": {  
          "get": {  
            "tags": [  
              "PopularToppics"  
      ],  
      "summary": "",  
      "description": "",  
      "consumes": [  
              "application/json"  
      ],  
      "produces": [  
              "application/json"  
      ],  
      "parameters": [  
      
            ],  
      "responses": {  
              "200": {  
                "description": "returns list of popular topics",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                  "Content-Type": {  
                    "description": "",  
      "type": "string",  
      "x-example-value": "application/json; charset=utf-8"  
      }  
                },  
      "examples": {  
                }  
              }  
            },  
      "deprecated": false,  
      "security": [  
      
            ]  
          },  
      "post": {  
            "tags": [  
              "Popular Topics"  
      ],  
      "summary": "",  
      "description": "",  
      "consumes": [  
              "application/json"  
      ],  
      "produces": [  
              "application/json"  
      ],  
      "parameters": [  
      
            ],  
      "responses": {  
              "201": {  
                "description": "creates book",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                  "Content-Type": {  
                    "description": "",  
      "type": "string",  
      "x-example-value": "application/json; charset=utf-8"  
      }  
                },  
      "examples": {  
                }  
              },  
      "422": {  
                "description": "returns 422 when parameter name is missing",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                  "Content-Type": {  
                    "description": "",  
      "type": "string",  
      "x-example-value": "application/json; charset=utf-8"  
      }  
                },  
      "examples": {  
                }  
              }  
            },  
      "deprecated": false,  
      "security": [  
      
            ]  
          }  
        },  
      "/popular_topics/{id}": {  
          "get": {  
            "tags": [  
              "Popular Topics"  
      ],  
      "summary": "",  
      "description": "",  
      "consumes": [  
              "application/json"  
      ],  
      "produces": [  
              "application/json"  
      ],  
      "parameters": [  
              {  
                "name": "id",  
      "in": "path",  
      "description": "",  
      "required": true,  
      "type": "integer"  
      }  
            ],  
      "responses": {  
              "200": {  
                "description": "returns book",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                  "Content-Type": {  
                    "description": "",  
      "type": "string",  
      "x-example-value": "application/json; charset=utf-8"  
      }  
                },  
      "examples": {  
                }  
              },  
      "404": {  
                "description": "returns 404 when book not found",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                  "Content-Type": {  
                    "description": "",  
      "type": "string",  
      "x-example-value": "application/json; charset=utf-8"  
      }  
                },  
      "examples": {  
                }  
              }  
            },  
      "deprecated": false,  
      "security": [  
      
            ]  
          },  
      "put": {  
            "tags": [  
              "Popular Topics"  
      ],  
      "summary": "",  
      "description": "",  
      "consumes": [  
              "application/json"  
      ],  
      "produces": [  
              "application/json"  
      ],  
      "parameters": [  
              {  
                "name": "id",  
      "in": "path",  
      "description": "",  
      "required": true,  
      "type": "integer"  
      }  
            ],  
      "responses": {  
              "200": {  
                "description": "updates book",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                  "Content-Type": {  
                    "description": "",  
      "type": "string",  
      "x-example-value": "application/json; charset=utf-8"  
      }  
                },  
      "examples": {  
                }  
              },  
      "404": {  
                "description": "returns 404 when book not found",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                  "Content-Type": {  
                    "description": "",  
      "type": "string",  
      "x-example-value": "application/json; charset=utf-8"  
      }  
                },  
      "examples": {  
                }  
              }  
            },  
      "deprecated": false,  
      "security": [  
      
            ]  
          }  
        },  
      "/random_verses/": {  
          "get": {  
            "tags": [  
              "Random Verses"  
      ],  
      "summary": "",  
      "description": "",  
      "consumes": [  
              "application/json"  
      ],  
      "produces": [  
              "application/json"  
      ],  
      "parameters": [  
      
            ],  
      "responses": {  
              "200": {  
                "description": "returns last random verse",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                  "Content-Type": {  
                    "description": "",  
      "type": "string",  
      "x-example-value": "application/json; charset=utf-8"  
      }  
                },  
      "examples": {  
                }  
              }  
            },  
      "deprecated": false,  
      "security": [  
      
            ]  
          }  
        },  
      "/random_verses/{id}": {  
          "get": {  
            "tags": [  
              "Random Verse"  
      ],  
      "summary": "",  
      "description": "",  
      "consumes": [  
              "application/json"  
      ],  
      "produces": [  
              "application/json"  
      ],  
      "parameters": [  
              {  
                "name": "id",  
      "in": "path",  
      "description": "",  
      "required": true,  
      "type": "string"  
      }  
            ],  
      "responses": {  
              "200": {  
                "description": "returns book",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                  "Content-Type": {  
                    "description": "",  
      "type": "string",  
      "x-example-value": "application/json; charset=utf-8"  
      }  
                },  
      "examples": {  
                }  
              },  
      "409": {  
                "description": "returns 404 when random verse not found",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                  "Content-Type": {  
                    "description": "",  
      "type": "string",  
      "x-example-value": "application/json; charset=utf-8"  
      }  
                },  
      "examples": {  
                }  
              }  
            },  
      "deprecated": false,  
      "security": [  
      
            ]  
          }  
        },  
      "/reading_plans/": {  
          "post": {  
            "tags": [  
              "Reading Plans"  
      ],  
      "summary": "",  
      "description": "",  
      "consumes": [  
              "application/json"  
      ],  
      "produces": [  
              "application/json"  
      ],  
      "parameters": [  
      
            ],  
      "responses": {  
              "201": {  
                "description": "creates reading plan",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                  "Content-Type": {  
                    "description": "",  
      "type": "string",  
      "x-example-value": "application/json; charset=utf-8"  
      }  
                },  
      "examples": {  
                }  
              }  
            },  
      "deprecated": false,  
      "security": [  
      
            ]  
          }  
        },  
      "/reading_plans/unsubscribe": {  
          "get": {  
            "tags": [  
              "Reading Plans"  
      ],  
      "summary": "",  
      "description": "",  
      "consumes": [  
              "application/json"  
      ],  
      "produces": [  
      
            ],  
      "parameters": [  
      
            ],  
      "responses": {  
              "204": {  
                "description": "creates reading plan",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                }  
              }  
            },  
      "deprecated": false,  
      "security": [  
      
            ]  
          }  
        },  
      "/subscriptions/": {  
          "post": {  
            "tags": [  
              "Subscriptions"  
      ],  
      "summary": "",  
      "description": "",  
      "consumes": [  
              "application/json"  
      ],  
      "produces": [  
              "application/json"  
      ],  
      "parameters": [  
      
            ],  
      "responses": {  
              "201": {  
                "description": "creates subscription",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                  "Content-Type": {  
                    "description": "",  
      "type": "string",  
      "x-example-value": "application/json; charset=utf-8"  
      }  
                },  
      "examples": {  
                }  
              }  
            },  
      "deprecated": false,  
      "security": [  
      
            ]  
          }  
        },  
      "/subscriptions/unsubscribe": {  
          "get": {  
            "tags": [  
              "Subscriptions"  
      ],  
      "summary": "",  
      "description": "",  
      "consumes": [  
              "application/json"  
      ],  
      "produces": [  
      
            ],  
      "parameters": [  
      
            ],  
      "responses": {  
              "204": {  
                "description": "creates reading plan",  
      "schema": {  
                  "description": "",  
      "type": "object",  
      "properties": {  
                  }  
                },  
      "headers": {  
                }  
              }  
            },  
      "deprecated": false,  
      "security": [  
      
            ]  
          }  
        }  
      },  
      "tags": [  
        {  
          "name": "Bibles",  
      "description": ""  
      },  
      {  
          "name": "Books",  
      "description": ""  
      },  
      {  
          "name": "Genres",  
      "description": ""  
      },  
      {  
          "name": "Popular Topics",  
      "description": ""  
      },  
      {  
          "name": "PopularToppics",  
      "description": ""  
      },  
      {  
          "name": "Random Verses",  
      "description": ""  
      },  
      {  
          "name": "Random Verse",  
      "description": ""  
      },  
      {  
          "name": "Reading Plans",  
      "description": ""  
      },  
      {  
          "name": "Subscriptions",  
      "description": ""  
      }  
      ]  
    }
