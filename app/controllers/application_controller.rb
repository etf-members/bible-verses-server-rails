class ApplicationController < ActionController::API
  include Authenticatable
  include ErrorRescuable
  include Pundit
end
