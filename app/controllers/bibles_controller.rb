class BiblesController < ApplicationController
  before_action :set_bible, only: [:show]

  def index
    @bibles = policy_scope(Bible)

    render json: @bibles, status: :ok
  end

  def show
    render json: @bible, status: :ok
  end

  def search
    @bibles = Bible.search(params)

    render json: @bibles, status: :ok
  end

  def search_by_book_or_verse
    @bibles = Bible.search_by_book_or_verse(params)

    render json: @bibles, status: :ok
  end

  def send_email
    UserMailer.contact_us(contact_us_parameters).deliver_now

    render json: { }, status: :ok
  end

  def random_verse
    @bible = Bible.random_verse(params)

    render json: @bible, status: :ok
  end

  def book_chapter_verses
    @bibles = Bible.where(book_id: params[:book_id], chapter: params[:chapter_id])

    render json: @bibles, status: :ok
  end

  def related_verses
    @bibles = Bible.where(book_id: params[:book_id])

    render json: @bibles, status: :ok
  end

  def book
    book = params[:book]
    @bibles = Bible.where(book_id: Book.where(slug: book).pluck(:id))

    render json: @bibles, status: :ok
  end

  def genre
    genre_id = params[:genre_id]
    @bibles = Bible.where(book_id: Book.where(genre_id: genre_id).pluck(:id))

    render json: @bibles, status: :ok
  end

  def old_testament
    @bibles = Bible.where(book_id: Book.where(testament: 'ot').pluck(:id))

    render json: @bibles, status: :ok
  end

  def new_testament
    @bibles = Bible.where(book_id: Book.where(testament: 'nt').pluck(:id))

    render json: @bibles, status: :ok
  end

  private

  def contact_us_parameters
    params.permit(:first_name, :last_name, :email, :phone_number, :message)
  end

  def set_bible
    @bible = Bible.where(slug: params[:id]).first
    @bible = Bible.find(params[:id]) unless @bible.present?
  end
end