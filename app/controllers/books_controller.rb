class BooksController < ApplicationController
  before_action :set_book, only: [:show]

  def index
    @books = policy_scope(Book)

    render json: @books, status: :ok
  end

  def show
    render json: @book, status: :ok
  end

  def genre
    genre_id = params[:genre_id]
    @books = Book.where(genre_id: genre_id)

    render json: @books, status: :ok
  end

  def old_testament
    @books = Book.where(testament: 'ot')

    render json: @books, status: :ok
  end

  def new_testament
    @books = Book.where(testament: 'nt')

    render json: @books, status: :ok
  end

  private

  def set_book
    @book = Book.find(params[:id])
  end
end
