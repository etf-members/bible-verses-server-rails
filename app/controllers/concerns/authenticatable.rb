module Authenticatable
  extend ActiveSupport::Concern
  include ActionController::HttpAuthentication::Token::ControllerMethods

  def authenticate_user
    authenticate_with_http_token do |token|
      @current_user = Security::TokenEncoder.decode(token)
    end
    raise AuthenticationFailedError if @current_user.nil?
  end

  def current_user
    @current_user
  end
end