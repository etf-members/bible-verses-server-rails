module ErrorRescuable
  extend ActiveSupport::Concern

  included do
    rescue_from ApplicationError, with: :handle_api_error

    rescue_from ActiveRecord::RecordInvalid, with: :handle_record_invalid
    rescue_from ActiveRecord::RecordNotFound, with: :handle_record_not_found
    rescue_from ActiveRecord::RecordNotUnique, with: :handle_not_unique_error
    rescue_from ActiveRecord::StatementInvalid, with: :handle_statement_invalid
  end

  def handle_api_error(error)
    render json: error.to_json, status: error.status
  end

  def handle_record_invalid(error)
    handle_api_error(ApplicationError.new(error.message, :unprocessable_entity, error.record.errors))
  end

  def handle_record_not_found(error)
    handle_api_error(ApplicationError.new(error.message, :not_found))
  end

  def handle_not_unique_error(error)
    handle_api_error(ApplicationError.new("Record is not unique!", :conflict))
  end

  def handle_statement_invalid(error)
    handle_api_error(ApplicationError.new("Invalid statement! (possible bad parameter)", :conflict))
  end
end