class GenresController < ApplicationController
  before_action :set_genre, only: [:show]

  def index
    @genres = policy_scope(Genre)

    render json: @genres, status: :ok
  end

  def show
    render json: @genre, status: :ok
  end

  private

  def set_genre
    @genre = Genre.find(params[:id])
  end

end
