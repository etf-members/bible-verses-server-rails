class PopularTopicsController < ApplicationController
  before_action :set_popular_topic, only: [:show, :update]

  def index
    @popular_topics = policy_scope(PopularTopic)

    render json: @popular_topics, status: :ok
  end

  def show
    render json: @popular_topic, status: :ok
  end

  def create
    @popular_topic = PopularTopic.create(popular_topic_params)

    render json: @popular_topic, status: :created
  end

  def update
    @popular_topic.update(popular_topic_params)

    render json: @popular_topic, status: :ok
  end

  private

  def popular_topic_params
    params.permit(:name)
  end

  def set_popular_topic
    @popular_topic = PopularTopic.find(params[:id])
  end
end
