class RandomVersesController < ApplicationController
  before_action :set_random_verse, only: [:show]

  def index
    @random_verses = policy_scope(RandomVerse)

    render json: @random_verses, status: :ok
  end

  def show
    render json: @random_verse, status: :ok
  end

  private

  def set_random_verse
    @random_verse = RandomVerse.where("DATE(created_at) = '#{params[:id]}'").first
    raise ActiveRecord::RecordNotFound if @random_verse.nil?
  end
end
