class ReadingPlansController < ApplicationController
  before_action :check_token, only: [:unsubscribe]

  def create
    raise ActiveRecord::RecordNotUnique unless ReadingPlan.where(email: create_params[:email], active: true).empty?

    @reading_plan = ReadingPlan.new(create_params)
    authorize(@reading_plan)

    @reading_plan.save!
    render json: @reading_plan, status: :created
  end

  def unsubscribe
    Actions::ReadingPlans::Destroy.execute({ reading_plan: @reading_plan })

    render json: { }, status: :no_content
  end

  private

  def create_params
    params.permit(:email, :date_from, :date_to, :monday, :tuesday, :wednesday, :thursday, :friday, :saturday, :sunday)
  end

  def check_token
    @reading_plan = Security::TokenEncoder.decode_reading_plan(params[:token])
    raise ActiveRecord::RecordNotFound if @reading_plan.nil?
  end

end
