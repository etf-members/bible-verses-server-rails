class SubscriptionsController < ApplicationController
  before_action :check_token, only: [:unsubscribe]

  def create
    @subscription = Actions::Subscriptions::Create.execute(subscription_params)

    render json: @subscription, status: :created
  end

  def unsubscribe
    Actions::Subscriptions::Destroy.execute({ subscription: @subscription })

    render json: { }, status: :no_content
  end

  private

  def subscription_params
    params.permit(:email)
  end

  def check_token
    @subscription = Security::TokenEncoder.decode_subscription(params[:token])
    raise ActiveRecord::RecordNotFound if @subscription.nil?
  end
end
