class RandomVerseJob < ApplicationJob
  def perform
    RandomVerse.create(bible_id: Bible.offset(rand(Bible.count)).first.id)
    RandomVerseJob.set(queue: "RandomVerseJob", wait: RandomVerseJob.wait_time_until_six_am).perform_later
  end

  def self.wait_time_until_six_am
    date_time_now = DateTime.now
    time_now = date_time_now.strftime("%H%M").to_i
    time_now > 600 ? eleven_am = 3000 : eleven_am = 600 # 24:00 -> 2400 | 11:00 -> 1100 | 2400+1100 = 3500

    difference = (eleven_am - time_now - 40).to_s.rjust(4, '0') # - 40 because of 60 minutes (100 - 40 = 60)
    hours_difference = (difference[0] + difference[1]).to_i
    minutes_difference = (difference[2] + difference[3]).to_i

    hours_difference.hours + minutes_difference.minutes
  end
end

