class ReadingPlanJob < ApplicationJob
  def perform
    reading_plans = ReadingPlan.where(active: true)

    reading_plans.each do |reading_plan|
      verses_per_day = calculate_verses_per_day(reading_plan)
      today = Date.today.strftime('%A').downcase
      token = Security::TokenEncoder.encode_reading_plan(reading_plan)

      unless reading_plan[today] == false
        verses_from = reading_plan.current_verse
        verses_to = verses_from + verses_per_day
        verses_to = Bible.count unless verses_to <= Bible.count
        verses_to_send = Bible.where(id: verses_from..verses_to)
        percent_complete = (verses_to*100) / Bible.count

        reading_plan.active = false if verses_to > Bible.count
        reading_plan.current_verse += verses_per_day
        reading_plan.save

        UserMailer.send_reading_plan_verses({
                                                email: reading_plan.email,
                                                verses: verses_to_send,
                                                token: token,
                                                percent_complete: percent_complete
                                            }.to_json).deliver_now
      end
    end

    ReadingPlanJob.set(queue: "ReadingPlanJob", wait: ReadingPlanJob.wait_time_until_eleven_am).perform_later
  end

  def calculate_verses_per_day(reading_plan)
    total_verses = Bible.count

    selected_days_count = 0
    selected_days_count += 1 unless reading_plan.monday == false
    selected_days_count += 1 unless reading_plan.tuesday == false
    selected_days_count += 1 unless reading_plan.wednesday == false
    selected_days_count += 1 unless reading_plan.thursday == false
    selected_days_count += 1 unless reading_plan.friday == false
    selected_days_count += 1 unless reading_plan.saturday == false
    selected_days_count += 1 unless reading_plan.sunday == false

    days_difference = (reading_plan.date_to - reading_plan.date_from).to_i
    weeks_difference = ((reading_plan.date_to - reading_plan.date_from) / 7).round
    total_days = days_difference - (7 - selected_days_count) * weeks_difference

    total_verses / total_days
  end

  def self.wait_time_until_eleven_am
    date_time_now = DateTime.now
    time_now = date_time_now.strftime("%H%M").to_i
    time_now > 1100 ? eleven_am = 3500 : eleven_am = 1100 # 24:00 -> 2400 | 11:00 -> 1100 | 2400+1100 = 3500

    difference = (eleven_am - time_now - 40).to_s.rjust(4, '0') # - 40 because of 60 minutes (100 - 40 = 60)
    hours_difference = (difference[0] + difference[1]).to_i
    minutes_difference = (difference[2] + difference[3]).to_i

    hours_difference.hours + minutes_difference.minutes
  end
end

