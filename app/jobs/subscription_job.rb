class SubscriptionJob < ApplicationJob
  def perform
    subscriptions = Subscription.all

    subscriptions.each do |subscription|
      random_verse = RandomVerse.last
      bible_verse = Bible.find(random_verse.bible_id)
      book = Book.find(bible_verse.book.id)
      token = Security::TokenEncoder.encode_subscription(subscription)

      UserMailer.send_subscription_verse({
                                             email: subscription.email,
                                             verse: bible_verse,
                                             book: book,
                                             token: token
                                         }.to_json).deliver_now
    end

    SubscriptionJob.set(queue: "SubscriptionJob", wait: ReadingPlanJob.wait_time_until_eleven_am).perform_later
  end
end

