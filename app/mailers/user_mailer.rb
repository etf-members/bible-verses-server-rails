class UserMailer < ApplicationMailer
  default from: '032.markoj96@gmail.com'

  def send_initial_reading_plan(data)
    data_json = JSON.parse(data, object_class: OpenStruct)
    @email = data_json['email']
    @date_from = data_json['date_from']
    @date_to = data_json['date_to']
    @days = data_json['days']
    @token = data_json['token']

    mail to: @email, subject: "Welcome to your BibleVerses.net Bible Reading Plan!", content_type: "text/html"
  end

  def send_reading_plan_verses(data)
    data_json = JSON.parse(data, object_class: OpenStruct)
    @email = data_json['email']
    @verses = data_json['verses']
    @token = data_json['token']
    @percent_complete = data_json['percent_complete']

    mail to: @email, subject: "BibleVerses.net #{@percent_complete}% Bible Progress Reading Plan", content_type: "text/html"
  end

  def send_initial_subscription(data)
    data_json = JSON.parse(data, object_class: OpenStruct)
    @email = data_json['email']
    @token = data_json['token']

    mail to: @email, subject: 'Welcome to BibleVerses.net Daily Verses!', content_type: "text/html"
  end

  def send_subscription_verse(data)
    data_json = JSON.parse(data, object_class: OpenStruct)
    @email = data_json['email']
    @verse = data_json['verse']
    @book = data_json['book']
    @token = data_json['token']

    mail to: @email, subject: "Daily Verse from BibleVerses.net - #{@book.full_name} #{@verse.chapter}:#{@verse.verse}", content_type: "text/html"
  end

  def contact_us(data)
    @to = '032.marcus@gmail.com'
    @from = data[:email]
    @first_name = data[:first_name]
    @last_name = data[:last_name]
    @phone_number = data[:phone_number]
    @message = data[:message]

    mail to: '032.marcus@gmail.com', subject: "Message from Bible verses", content_type: "text/html"
  end
end

