class Bible < ApplicationRecord
  belongs_to :book

  def self.search(params)
    Actions::Bibles::Search.execute(params)
  end

  def self.search_by_book_or_verse(params)
    Actions::Bibles::SearchByBookOrVerse.execute(params)
  end

  def self.random_verse(params)
    Actions::Bibles::RandomVerse.execute(params)
  end
end
