class PopularTopic < ApplicationRecord
  validates :name, presence: true

  def self.create(params)
    Actions::PopularTopics::Create.execute(params)
  end

  def self.update(params)
    Actions::PopularTopics::Update.execute(params)
  end
end
