class ReadingPlan < ApplicationRecord
  validates :email, presence: true
  validates :date_from, presence: true
  validates :date_to, presence: true

  after_create :send_initial_reading_plan_mail

  def send_initial_reading_plan_mail

    UserMailer.send_initial_reading_plan({
                                             email: self.email,
                                             date_from: self.date_from,
                                             date_to: self.date_to,
                                             days: selected_days_abbreviations(self),
                                             token: generate_unsubscribe_token(self)
                                         }.to_json).deliver_now
  end

  def selected_days_abbreviations(reading_plan)
    selected_days = []
    selected_days.push('Mon') unless reading_plan.monday.nil?
    selected_days.push('Tue') unless reading_plan.tuesday.nil?
    selected_days.push('Wed') unless reading_plan.wednesday.nil?
    selected_days.push('Thu') unless reading_plan.thursday.nil?
    selected_days.push('Fri') unless reading_plan.friday.nil?
    selected_days.push('Sat') unless reading_plan.saturday.nil?
    selected_days.push('Sun') unless reading_plan.sunday.nil?

    selected_days
  end

  def generate_unsubscribe_token(reading_plan)
    Security::TokenEncoder.encode_reading_plan(reading_plan)
  end
end
