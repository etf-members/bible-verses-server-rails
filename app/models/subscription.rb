class Subscription < ApplicationRecord
  validates :email, presence: true

  after_create :send_initial_subscription_mail

  def send_initial_subscription_mail
    UserMailer.send_initial_subscription({ email: self.email, token: generate_unsubscribe_token(self) }.to_json).deliver_now
  end

  def generate_unsubscribe_token(subscription)
    Security::TokenEncoder.encode_subscription(subscription)
  end
end
