class BibleSerializer < ActiveModel::Serializer
  attribute :id
  attribute :index
  attribute :slug
  # has_one :book
  attribute :book_id
  attribute :chapter
  attribute :verse
  attribute :text
end
