class BookSerializer < ActiveModel::Serializer
  attribute :id
  attribute :full_name
  attribute :slug
  attribute :short
  attribute :search
  attribute :chapters
  attribute :title
  attribute :testament
  has_one :genre
end
