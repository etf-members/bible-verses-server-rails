class GenreSerializer < ActiveModel::Serializer
  attribute :id
  attribute :name
end
