class RandomVerseSerializer < ActiveModel::Serializer
  attribute :id
  attribute :bible_id
  attribute :created_at
end
