class ReadingPlanSerializer < ActiveModel::Serializer
  attribute :id
  attribute :email
  attribute :date_from
  attribute :date_to
  attribute :monday
  attribute :tuesday
  attribute :wednesday
  attribute :thursday
  attribute :friday
  attribute :saturday
  attribute :sunday
  attribute :active
  attribute :current_verse
end
