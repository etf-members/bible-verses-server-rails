class SubscriptionSerializer < ActiveModel::Serializer
  attribute :id
  attribute :email
end
