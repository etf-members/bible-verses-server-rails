module Actions
  module Bibles

    class RandomVerse < ApplicationAction

      def initialize(params)
        super(params)

        @verse_text = params[:text]
      end

      def execute
        verses = Bible.where('text LIKE ?', "%#{@verse_text}%")
        verses.offset(rand(verses.count)).first
      end

    end
  end
end