module Actions
  module Bibles

    class Search < ApplicationAction

      def initialize(params)
        super(params)

        @verse_text = params[:text]
      end

      def execute
        popular_topics = PopularTopic.where(name: @verse_text)

        if popular_topics.any?
          Rails.cache.fetch("verses_search_#{@verse_text}") do
            JSON.parse(Bible.where('text LIKE ?', "%#{@verse_text}%").to_json, object_class: OpenStruct)
          end
        else
          Rails.cache.fetch("verses_search_#{@verse_text}", expires_in: 1.day) do
            JSON.parse(Bible.where('text LIKE ?', "%#{@verse_text}%").to_json, object_class: OpenStruct)
          end
        end
      end

    end
  end
end