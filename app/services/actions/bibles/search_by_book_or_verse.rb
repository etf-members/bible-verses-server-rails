module Actions
  module Bibles

    class SearchByBookOrVerse < ApplicationAction

      def initialize(params)
        super(params)

        @search_param = params[:text]
        @search_book = nil
      end

      def execute
        Book.all.each do |book|
          if @search_param.downcase.include? book.full_name.downcase
            @search_book = book
            break
          end
        end

        if @search_book.nil?
          []
        elsif @search_param.downcase.split(@search_book.full_name.downcase).count > 1
          chapter_verse = @search_param.downcase.split(@search_book.full_name.downcase).last.split(':')
          chapter = chapter_verse.first unless chapter_verse.nil?
          verse = chapter_verse.last unless chapter_verse.nil?

          Rails.cache.fetch("bible_search_by_book_or_verses#{@search_param}", expires_in: 1.day) do
            JSON.parse(Bible.where(book_id: @search_book.id, chapter: chapter, verse: verse).to_json, object_class: OpenStruct)
          end
        else
          Rails.cache.fetch("bible_search_by_book_or_verses#{@search_param}", expires_in: 1.day) do
            JSON.parse(Bible.where(book_id: @search_book.id).to_json, object_class: OpenStruct)
          end
        end
      end

    end
  end
end