module Actions
  module PopularTopics
    class Create < ApplicationAction

      def initialize(params)
        super(params)
      end

      def execute
        PopularTopic.create!(params)
      end

    end
  end
end
