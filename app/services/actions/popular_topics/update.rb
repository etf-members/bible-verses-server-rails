module Actions
  module PopularTopics
    class Update < ApplicationAction

      def initialize(params)
        super(params)
      end

      def execute
        PopularTopics.update!(params)
      end

    end
  end
end
