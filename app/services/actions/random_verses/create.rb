module Actions
  module RandomVerses
    class Create < ApplicationAction

      def initialize
        super(params)
      end

      def execute
        RandomVerse.create!(params)
      end

    end
  end
end