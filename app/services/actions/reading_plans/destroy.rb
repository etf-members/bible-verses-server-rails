module Actions
  module ReadingPlans
    class Destroy < ApplicationAction

      def initialize(params)
        super(params)
      end

      def execute
        reading_plan = params[:reading_plan]
        reading_plan.destroy!
      end

    end
  end
end