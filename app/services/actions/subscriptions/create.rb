module Actions
  module Subscriptions
    class Create < ApplicationAction

      def initialize(params)
        super(params)
      end

      def execute
        Subscription.create!(params)
      end

    end
  end
end