module Actions
  module Subscriptions
    class Destroy < ApplicationAction

      def initialize(params)
        super(params)
      end

      def execute
        subscription = params[:subscription]
        subscription.destroy!
      end

    end
  end
end