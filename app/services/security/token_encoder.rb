module Security

  class TokenEncoder

    def self.decode_subscription(token)
      decoded = JWT.decode(token, Rails.application.secrets.secret_key_base, true, algorithm: 'HS256').first
      Subscription.find(decoded['subscription_id'])
    end

    def self.encode_subscription(subscription, expiration = nil)
      payload = { subscription_id: subscription.id }
      payload[:exp] = Time.now.to_i + expiration unless expiration.nil?

      JWT.encode(payload, Rails.application.secrets.secret_key_base, 'HS256')
    end

    def self.decode_reading_plan(token)
      decoded = JWT.decode(token, Rails.application.secrets.secret_key_base, true, algorithm: 'HS256').first
      ReadingPlan.find(decoded['reading_plan_id'])
    end

    def self.encode_reading_plan(reading_plan, expiration = nil)
      payload = { reading_plan_id: reading_plan.id }
      payload[:exp] = Time.now.to_i + expiration unless expiration.nil?

      JWT.encode(payload, Rails.application.secrets.secret_key_base, 'HS256')
    end
  end

end
