Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  #
  resources :books, only: [:index, :show] do
    get :old_testament, on: :collection
    get :new_testament, on: :collection
    get :genre,         on: :collection
  end

  resources :bibles, only: [:index, :show] do
    get :search, on: :collection
    get :search_by_book_or_verse, on: :collection
    get :book, on: :collection
    get :genre, on: :collection
    get :old_testament, on: :collection
    get :new_testament, on: :collection
    post :send_email, on: :collection
    get :random_verse, on: :collection
    get :book_chapter_verses, on: :collection
    get :related_verses, on: :collection
  end

  resources :genres, only: [:index, :show]

  resources :popular_topics

  resources :reading_plans, only: [:create] do
    get :unsubscribe, on: :collection
  end

  resources :subscriptions, only: [:create] do
    get :unsubscribe, on: :collection
  end

  resources :random_verses, only: [:index, :show]
end
