class CreateBooks < ActiveRecord::Migration[6.0]
  def change
    create_table :books do |t|
      t.text :full_name, null: false
      t.string :slug, null: true
      t.string :short, null: false
      t.string :search, null: true
      t.integer :chapters, null: false
      t.text :title, null: true
      t.string :testament, null: false
      t.references :genre, null: false
    end
  end
end
