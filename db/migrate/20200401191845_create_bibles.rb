class CreateBibles < ActiveRecord::Migration[6.0]
  def change
    create_table :bibles do |t|
      t.integer :index, null: false
      t.string :slug, null: true
      t.references :book, null: false
      t.integer :chapter, null: false
      t.integer :verse, null: false
      t.text :text, null: false
    end
  end
end
