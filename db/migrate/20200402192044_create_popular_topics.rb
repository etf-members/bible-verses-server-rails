class CreatePopularTopics < ActiveRecord::Migration[6.0]
  def change
    create_table :popular_topics do |t|
      t.string :name, null: false
    end
  end
end
