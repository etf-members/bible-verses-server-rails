class AddIndexToBibles < ActiveRecord::Migration[6.0]
  def change
    add_index :bibles, :index
    add_index :bibles, :chapter
    add_index :bibles, :verse
    add_index :bibles, :text, length: { text: 255 }
  end
end
