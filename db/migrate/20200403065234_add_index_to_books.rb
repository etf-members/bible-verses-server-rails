class AddIndexToBooks < ActiveRecord::Migration[6.0]
  def change
    add_index :books, :slug
    add_index :books, :short
    add_index :books, :search
  end
end
