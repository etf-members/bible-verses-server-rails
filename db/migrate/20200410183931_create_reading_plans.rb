class CreateReadingPlans < ActiveRecord::Migration[6.0]
  def change
    create_table :reading_plans do |t|
      t.string :email
      t.date :date_from 
      t.date :date_to 
      t.boolean :monday
      t.boolean :tuesday
      t.boolean :wednesday
      t.boolean :thursday
      t.boolean :friday
      t.boolean :saturday
      t.boolean :sunday
      t.boolean :active, default: true
      t.integer :current_verse, default: 1
    end
  end
end
