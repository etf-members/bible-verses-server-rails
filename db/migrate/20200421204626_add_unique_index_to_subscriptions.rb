class AddUniqueIndexToSubscriptions < ActiveRecord::Migration[6.0]
  def change
    add_index :subscriptions, :email, unique: true
  end
end
