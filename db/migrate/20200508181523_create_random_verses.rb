class CreateRandomVerses < ActiveRecord::Migration[6.0]
  def change
    create_table :random_verses do |t|
      t.belongs_to :bible
      t.timestamps
    end
  end
end