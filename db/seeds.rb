# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#


['bibles', 'books', 'genres', 'popular_topics'].each do |table|
  connection = ActiveRecord::Base.connection
  sql = File.read("db/imports/#{table}.sql")
  statements = sql.split(/;$/)
  statements.pop

  ActiveRecord::Base.transaction do
    statements.each do |statement|
      connection.execute(statement)
    end
  end
end

RandomVerseJob.set(queue: "RandomVerseJob", wait: 1.minute).perform_later
ReadingPlanJob.set(queue: "ReadingPlanJob", wait: 1.minute).perform_later
ReadingPlanJob.set(queue: "SubscriptionJob", wait: 1.minute).perform_later

