FactoryBot.define do
  factory :bible, class: Bible do
    index { Faker::Number.number(digits: 2) }
    slug { Faker::Lorem.word }
    book { create(:book) }
    chapter { Faker::Number.number(digits: 1) }
    verse { Faker::Number.number(digits: 1) }
    text { Faker::Lorem.words(number: 5) }
  end
end
