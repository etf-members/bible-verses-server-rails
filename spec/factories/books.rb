FactoryBot.define do
  factory :book, class: Book do
    full_name { Faker::Lorem.words(number: 5) }
    slug { Faker::Lorem.word }
    short { Faker::Lorem.word }
    search { Faker::Lorem.word }
    chapters { Faker::Number.number(digits: 2) }
    title { Faker::Lorem.words(number: 5) }
    genre { create(:genre) }
    testament { Faker::Lorem.word }
  end
end
