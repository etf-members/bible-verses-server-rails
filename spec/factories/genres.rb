FactoryBot.define do
  factory :genre, class: Genre do
    name { Faker::Lorem.word }
  end
end
