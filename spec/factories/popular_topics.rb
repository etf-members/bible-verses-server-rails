FactoryBot.define do
  factory :popular_topic, class: PopularTopic do
    name { Faker::Lorem.word }
  end
end
