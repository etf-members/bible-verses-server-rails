FactoryBot.define do
  factory :random_verse, class: RandomVerse do
    bible { create(:bible) }
  end
end
