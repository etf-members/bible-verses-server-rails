FactoryBot.define do
  factory :reading_plan, class: ReadingPlan do
    email { Faker::Boolean.boolean(true_ratio: 0.5) }
    date_from { Faker::Date.between(from: Date.today, to: Date.today) }
    date_to { Faker::Date.between(from: 1.day.after, to: 1.year.after) }
    monday { Faker::Boolean.boolean(true_ratio: 0.5) }
    tuesday { Faker::Boolean.boolean(true_ratio: 0.5) }
    wednesday { Faker::Boolean.boolean(true_ratio: 0.5) }
    thursday { Faker::Boolean.boolean(true_ratio: 0.5) }
    friday { Faker::Boolean.boolean(true_ratio: 0.5) }
    saturday { Faker::Boolean.boolean(true_ratio: 0.5) }
    sunday { Faker::Boolean.boolean(true_ratio: 0.5) }
    active { true }
    current_verse { Faker::Number.decimal }
  end
end
