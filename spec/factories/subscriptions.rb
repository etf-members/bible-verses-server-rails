FactoryBot.define do
  factory :subscription, class: Subscription do
    email { Faker::Internet.email }
  end
end
