require 'rails_helper'

RSpec.describe BiblesController, type: :request do
  resource 'Bibles' do
    get '/bibles/' do
      header "Content-Type", "application/json"

      context 'Successful' do
        context 'Bibles exists' do
          let!(:bibles) {[create(:bible), create(:bible), create(:bible)]}
          it "returns list of bibles" do
            do_request

            bibles_response = JSON.parse(response_body, object_class: OpenStruct)
            expect(status).to eq 200
            expect(bibles_response.length).to eq 3
            expect(bibles_response[0].id).to eq bibles[0].id
            expect(bibles_response[0].index).to eq bibles[0].index
            expect(bibles_response[0].slug).to eq bibles[0].slug
            expect(bibles_response[0].book_id).to eq bibles[0].book.id
            expect(bibles_response[0].verse).to eq bibles[0].verse
            expect(bibles_response[0].chapter).to eq bibles[0].chapter
            expect(bibles_response[0].text).to eq bibles[0].text

            expect(bibles_response[1].id).to eq bibles[1].id
            expect(bibles_response[1].index).to eq bibles[1].index
            expect(bibles_response[1].slug).to eq bibles[1].slug
            expect(bibles_response[1].book_id).to eq bibles[1].book.id
            expect(bibles_response[1].verse).to eq bibles[1].verse
            expect(bibles_response[1].chapter).to eq bibles[1].chapter
            expect(bibles_response[1].text).to eq bibles[1].text

            expect(bibles_response[2].id).to eq bibles[2].id
            expect(bibles_response[2].index).to eq bibles[2].index
            expect(bibles_response[2].slug).to eq bibles[2].slug
            expect(bibles_response[2].book_id).to eq bibles[2].book.id
            expect(bibles_response[2].verse).to eq bibles[2].verse
            expect(bibles_response[2].chapter).to eq bibles[2].chapter
            expect(bibles_response[2].text).to eq bibles[2].text
          end
        end

        context 'Bibles non-exists' do
          it "returns empty list when there are no bibles" do
            do_request

            bibles_response = JSON.parse(response_body, object_class: OpenStruct)
            expect(status).to eq 200
            expect(bibles_response.length).to eq 0
          end
        end
      end
    end
  end
end