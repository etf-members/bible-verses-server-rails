require 'rails_helper'

RSpec.describe BiblesController, type: :request do
  resource 'Bibles' do
    get '/bibles/:id' do
      header "Content-Type", "application/json"

      context 'Successful' do
        let(:bible) { create(:bible) }
        let(:id) { bible.id }

        it "returns bibles" do
          do_request

          bible_response = JSON.parse(response_body, object_class: OpenStruct)
          expect(status).to eq 200
          expect(bible_response.id).to eq bible.id
          expect(bible_response.index).to eq bible.index
          expect(bible_response.slug).to eq bible.slug
          expect(bible_response.book_id).to eq bible.book.id
          expect(bible_response.chapter).to eq bible.chapter
          expect(bible_response.verse).to eq bible.verse
          expect(bible_response.text).to eq bible.text
        end
      end

      context 'Unsuccessful' do
        let(:id) { 'wrong_id' }

        it "returns 404 when bibles not found" do
          do_request

          expect(status).to eq 404
        end
      end
    end
  end
end