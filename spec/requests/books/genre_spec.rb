require 'rails_helper'

RSpec.describe BooksController, type: :request do
  resource 'Books' do
    get '/books/genre' do
      header "Content-Type", "application/json"

      context 'Successful' do
        context 'Books exists' do
          context 'Genre exists' do
            let!(:books) {
              [
                  create(:book),
                  create(:book),
                  create(:book)
              ]
            }

            it "returns list of books" do
              params = {
                  genre_id: books.first.genre.id
              }
              do_request(params)

              books_response = JSON.parse(response_body, object_class: OpenStruct)
              expect(status).to eq 200
              expect(books_response.length).to eq 1
              expect(books_response[0].id).to eq books[0].id
              expect(books_response[0].full_name).to eq books[0].full_name
              expect(books_response[0].slug).to eq books[0].slug
              expect(books_response[0].short).to eq books[0].short
              expect(books_response[0].search).to eq books[0].search
              expect(books_response[0].chapters).to eq books[0].chapters
              expect(books_response[0].title).to eq books[0].title
              expect(books_response[0].testament).to eq books[0].testament
              expect(books_response[0].genre.id).to eq books[0].genre.id
            end
          end

          context 'Genre non-exists' do
            let!(:books) {
              [
                  create(:book),
                  create(:book),
                  create(:book)
              ]
            }

            it "returns empty list" do
              params = {
                  genre_id: 999
              }
              do_request(params)

              books_response = JSON.parse(response_body, object_class: OpenStruct)
              expect(status).to eq 200
              expect(books_response.length).to eq 0
            end
          end
        end

        context 'Books non-exists' do
          it "returns empty list when there are no books" do
            do_request

            books_response = JSON.parse(response_body, object_class: OpenStruct)
            expect(status).to eq 200
            expect(books_response.length).to eq 0
          end
        end
      end
    end
  end
end