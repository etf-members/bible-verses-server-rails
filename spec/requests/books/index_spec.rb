require 'rails_helper'

RSpec.describe BooksController, type: :request do
  resource 'Books' do
    get '/books/' do
      header "Content-Type", "application/json"

      context 'Successful' do
        context 'Books exists' do
          let!(:books) {[create(:book), create(:book), create(:book)]}
          it "returns list of books" do
            do_request

            books_response = JSON.parse(response_body, object_class: OpenStruct)
            expect(status).to eq 200
            expect(books_response.length).to eq 3
            expect(books_response[0].id).to eq books[0].id
            expect(books_response[0].full_name).to eq books[0].full_name
            expect(books_response[0].slug).to eq books[0].slug
            expect(books_response[0].short).to eq books[0].short
            expect(books_response[0].search).to eq books[0].search
            expect(books_response[0].chapters).to eq books[0].chapters
            expect(books_response[0].title).to eq books[0].title
            expect(books_response[0].testament).to eq books[0].testament
            expect(books_response[0].genre.id).to eq books[0].genre.id

            expect(books_response[1].id).to eq books[1].id
            expect(books_response[1].full_name).to eq books[1].full_name
            expect(books_response[1].slug).to eq books[1].slug
            expect(books_response[1].short).to eq books[1].short
            expect(books_response[1].search).to eq books[1].search
            expect(books_response[1].chapters).to eq books[1].chapters
            expect(books_response[1].title).to eq books[1].title
            expect(books_response[1].testament).to eq books[1].testament
            expect(books_response[1].genre.id).to eq books[1].genre.id

            expect(books_response[2].id).to eq books[2].id
            expect(books_response[2].full_name).to eq books[2].full_name
            expect(books_response[2].slug).to eq books[2].slug
            expect(books_response[2].short).to eq books[2].short
            expect(books_response[2].search).to eq books[2].search
            expect(books_response[2].chapters).to eq books[2].chapters
            expect(books_response[2].title).to eq books[2].title
            expect(books_response[2].testament).to eq books[2].testament
            expect(books_response[2].genre.id).to eq books[2].genre.id
          end
        end

        context 'Books non-exists' do
          it "returns empty list when there are no books" do
            do_request

            books_response = JSON.parse(response_body, object_class: OpenStruct)
            expect(status).to eq 200
            expect(books_response.length).to eq 0
          end
        end
      end
    end
  end
end