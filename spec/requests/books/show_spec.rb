require 'rails_helper'

RSpec.describe BooksController, type: :request do
  resource 'Books' do
    get '/books/:id' do
      header "Content-Type", "application/json"

      context 'Successful' do
        let(:book) { create(:book) }
        let(:id) { book.id }

        it "returns book" do
          do_request

          books_response = JSON.parse(response_body, object_class: OpenStruct)
          expect(status).to eq 200
          expect(books_response.id).to eq book.id
          expect(books_response.full_name).to eq book.full_name
          expect(books_response.slug).to eq book.slug
          expect(books_response.short).to eq book.short
          expect(books_response.search).to eq book.search
          expect(books_response.chapters).to eq book.chapters
          expect(books_response.title).to eq book.title
          expect(books_response.testament).to eq book.testament
          expect(books_response.genre.id).to eq book.genre.id
        end
      end

      context 'Unsuccessful' do
        let(:id) { 'wrong_id' }

        it "returns 404 when book not found" do
          do_request

          expect(status).to eq 404
        end
      end
    end
  end
end