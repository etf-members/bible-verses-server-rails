require 'rails_helper'

RSpec.describe GenresController, type: :request do
  resource 'Genres' do
    get '/genres/:id' do
      header "Content-Type", "application/json"

      context 'Successful' do
        let(:genre) { create(:genre) }
        let(:id) { genre.id }

        it "returns genres" do
          do_request

          genre_response = JSON.parse(response_body, object_class: OpenStruct)
          expect(status).to eq 200
          expect(genre_response.id).to eq genre.id
          expect(genre_response.name).to eq genre.name
        end
      end

      context 'Unsuccessful' do
        let(:id) { 'wrong_id' }

        it "returns 404 when genres not found" do
          do_request

          expect(status).to eq 404
        end
      end
    end
  end
end