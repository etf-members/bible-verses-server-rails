require 'rails_helper'

RSpec.describe PopularTopicsController, type: :request do
  resource 'Popular Topics' do
    post '/popular_topics/' do
      header "Content-Type", "application/json"

      context 'Successful' do
        it "creates book" do
          params = {
              name: 'some topic name'
          }

          popular_topics_count_before = PopularTopic.count
          do_request(params)
          popular_topics_count_after = PopularTopic.count

          popular_topic_response = JSON.parse(response_body, object_class: OpenStruct)
          expect(status).to eq 201
          popular_topic = PopularTopic.find(popular_topic_response.id)
          expect(popular_topic.name).to eq params[:name]
          expect(popular_topics_count_after - 1).to eq popular_topics_count_before
        end
      end

      context 'Unsuccessful' do

        [:name].each do |param|
          it "returns 422 when parameter #{param} is missing" do
            params = {
                name: 'some topic_name'
            }.except(param)

            do_request(params)

            expect(status).to eq 422
          end
        end
      end
    end
  end
end