require 'rails_helper'

RSpec.describe PopularTopicsController, type: :request do
  resource 'PopularToppics' do
    get '/popular_topics/' do
      header "Content-Type", "application/json"

      context 'Successful' do
        context 'Popular topics exists' do
          let!(:popular_topics) {[create(:popular_topic), create(:popular_topic), create(:popular_topic)]}
          it "returns list of popular topics" do
            do_request

            popular_topics_response = JSON.parse(response_body, object_class: OpenStruct)
            expect(status).to eq 200
            expect(popular_topics_response.length).to eq 3
            expect(popular_topics_response[0].id).to eq popular_topics[0].id
            expect(popular_topics_response[0].name).to eq popular_topics[0].name

            expect(popular_topics_response[1].id).to eq popular_topics[1].id
            expect(popular_topics_response[1].name).to eq popular_topics[1].name

            expect(popular_topics_response[2].id).to eq popular_topics[2].id
            expect(popular_topics_response[2].name).to eq popular_topics[2].name
          end
        end

        context 'popular_topics non-exists' do
          it "returns empty list when there are no popular topics" do
            do_request

            popular_topics_response = JSON.parse(response_body, object_class: OpenStruct)
            expect(status).to eq 200
            expect(popular_topics_response.length).to eq 0
          end
        end
      end
    end
  end
end