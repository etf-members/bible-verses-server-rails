require 'rails_helper'

RSpec.describe PopularTopicsController, type: :request do
  resource 'Popular Topics' do
    get '/popular_topics/:id' do
      header "Content-Type", "application/json"

      context 'Successful' do
        let(:popular_topic) { create(:popular_topic) }
        let(:id) { popular_topic.id }

        it "returns book" do
          do_request

          popular_topic_response = JSON.parse(response_body, object_class: OpenStruct)
          expect(status).to eq 200
          expect(popular_topic_response.id).to eq popular_topic.id
          expect(popular_topic_response.name).to eq popular_topic.name
        end
      end

      context 'Unsuccessful' do
        let(:id) { 'wrong_id' }

        it "returns 404 when book not found" do
          do_request

          expect(status).to eq 404
        end
      end
    end
  end
end