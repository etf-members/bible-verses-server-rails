require 'rails_helper'

RSpec.describe RandomVersesController, type: :request do
  resource 'Random Verses' do
    get '/random_verses/' do
      header "Content-Type", "application/json"

      context 'Successful' do
        context 'RandomVerses exists' do
          let!(:random_verses) {[create(:random_verse), create(:random_verse), create(:random_verse)]}
          it "returns last random verse" do
            do_request

            random_verses_response = JSON.parse(response_body, object_class: OpenStruct)
            expect(status).to eq 200
            expect(random_verses_response.length).to eq 3
            expect(random_verses_response.first.id).to eq random_verses.first.id
            expect(random_verses_response.first.bible_id).to eq random_verses.first.bible.id
            expect(random_verses_response.second.id).to eq random_verses.second.id
            expect(random_verses_response.second.bible_id).to eq random_verses.second.bible.id
            expect(random_verses_response.third.id).to eq random_verses.third.id
            expect(random_verses_response.third.bible_id).to eq random_verses.third.bible.id
          end
        end

        context 'RandomVerses non-exists' do
          it "returns empty list when there are no random_verses" do
            do_request

            random_verses_response = JSON.parse(response_body, object_class: OpenStruct)
            expect(status).to eq 200
            expect(random_verses_response).to eq []
          end
        end
      end
    end
  end
end