require 'rails_helper'

RSpec.describe RandomVersesController, type: :request do
  resource 'Random Verse' do
    get '/random_verses/:id' do
      header "Content-Type", "application/json"

      context 'Successful' do
        let(:random_verse) { create(:random_verse) }
        let(:id) { random_verse.created_at.to_datetime.strftime("%y-%m-%d") }

        it "returns book" do
          do_request

          random_verse_response = JSON.parse(response_body, object_class: OpenStruct)
          expect(status).to eq 200
          expect(random_verse_response.id).to eq random_verse.id
          expect(random_verse_response.bible_id).to eq random_verse.bible_id
        end
      end

      context 'Unsuccessful' do
        let(:id) { 'wrong_id' }

        it "returns 404 when random verse not found" do
          do_request

          expect(status).to eq 409
        end
      end
    end
  end
end