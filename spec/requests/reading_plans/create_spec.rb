require 'rails_helper'

RSpec.describe ReadingPlansController, type: :request do
  resource 'Reading Plans' do
    post '/reading_plans/' do
      header "Content-Type", "application/json"

      context 'Successful' do
        it "creates reading plan" do
          params = {
              email: 'some@email.com',
              date_from: Faker::Date.between(from: 2.days.ago, to: Date.today),
              date_to: Faker::Date.between(from: 2.days.after, to: 7.days.after),
              monday: true,
              tuesday: true,
              wednesday: true,
              thursday: true,
              friday: true,
              saturday: false,
              sunday: false
          }

          reading_plans_count_before = ReadingPlan.count
          do_request(params)
          reading_plans_count_after = ReadingPlan.count

          reading_plan_response = JSON.parse(response_body, object_class: OpenStruct)
          expect(status).to eq 201
          reading_plan = ReadingPlan.find(reading_plan_response.id)
          expect(reading_plan.email).to eq params[:email]
          expect(reading_plan.date_from).to eq params[:date_from]
          expect(reading_plan.date_to).to eq params[:date_to]
          expect(reading_plan.monday).to eq params[:monday]
          expect(reading_plan.tuesday).to eq params[:tuesday]
          expect(reading_plan.wednesday).to eq params[:wednesday]
          expect(reading_plan.thursday).to eq params[:thursday]
          expect(reading_plan.friday).to eq params[:friday]
          expect(reading_plan.saturday).to eq params[:saturday]
          expect(reading_plan.sunday).to eq params[:sunday]
          expect(reading_plan.active).to eq true
          expect(reading_plan.current_verse).to eq 1
          expect(reading_plans_count_after - 1).to eq reading_plans_count_before
        end
      end
    end
  end
end