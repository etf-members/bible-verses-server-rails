require 'rails_helper'

RSpec.describe ReadingPlansController, type: :request do
  resource 'Reading Plans' do
    get '/reading_plans/unsubscribe' do
      header "Content-Type", "application/json"

      context 'Successful' do
        let(:reading_plan) { create(:reading_plan) }
        it "creates reading plan" do
          token = Security::TokenEncoder.encode_reading_plan(reading_plan)
          params = {
              token: token
          }
          reading_plans_count_before = ReadingPlan.count
          do_request(params)
          reading_plans_count_after = ReadingPlan.count

          expect(status).to eq 204
          expect(reading_plans_count_after + 1).to eq reading_plans_count_before
        end
      end
    end
  end
end