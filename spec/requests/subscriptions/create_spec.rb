require 'rails_helper'

RSpec.describe SubscriptionsController, type: :request do
  resource 'Subscriptions' do
    post '/subscriptions/' do
      header "Content-Type", "application/json"

      context 'Successful' do
        it "creates subscription" do
          params = {
              email: 'some@email.com'
          }

          subscriptions_count_before = Subscription.count
          do_request(params)
          subscriptions_count_after = Subscription.count

          subscription_response = JSON.parse(response_body, object_class: OpenStruct)
          expect(status).to eq 201
          subscription = Subscription.find(subscription_response.id)
          expect(subscription.email).to eq params[:email]
          expect(subscriptions_count_after - 1).to eq subscriptions_count_before
        end
      end
    end
  end
end