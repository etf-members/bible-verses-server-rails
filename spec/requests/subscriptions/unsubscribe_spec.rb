require 'rails_helper'

RSpec.describe SubscriptionsController, type: :request do
  resource 'Subscriptions' do
    get '/subscriptions/unsubscribe' do
      header "Content-Type", "application/json"

      context 'Successful' do
        let(:subscription) { create(:subscription) }
        it "creates reading plan" do
          token = Security::TokenEncoder.encode_subscription(subscription)
          params = {
              token: token
          }
          subscriptions_count_before = Subscription.count
          do_request(params)
          subscriptions_count_after = Subscription.count

          expect(status).to eq 204
          expect(subscriptions_count_after + 1).to eq subscriptions_count_before
        end
      end
    end
  end
end