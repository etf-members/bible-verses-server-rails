module ActionDispatch
  class TestResponse
    def normalize
      JSON.parse(body, object_class: OpenStruct)
    end
  end
end

RSpec::Matchers.define :date_eq do |expected|
  match do |actual|
    Date.parse(actual.to_s) == Date.parse(expected.to_s)
  end
end

def auth_headers(user)
  token = user.auth_token
  {
      'authorization': "Bearer #{token}",
      'accept': 'application/json',
      'content-type': 'application/json'
  }
end

def json_headers
  {
      'accept': 'application/json',
      'content-type': 'application/json'
  }
end